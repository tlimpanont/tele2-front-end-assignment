'use strict';

import {
  getJSON
}
from './ajax-loader';
import PhoneCatalog from './phone-catalog';
import PhoneSelector from './phone-selector';

getJSON('./phones.json')
  .then((phones) => {
    Promise.all([
        PhoneCatalog.loadAllImages(phones),
        PhoneSelector.render(phones),
        PhoneCatalog.render(phones)
      ])
      .then(() => {

        Array.from(document.querySelectorAll('.phone-item')).forEach((item) => {
          item.addEventListener('mouseover', (event) => {
            let target = event.currentTarget;
            let itemOverlay = target.querySelector('.item-overlay');
            let information = target.querySelector('.information');

            itemOverlay.className += ' animation';
            information.style.display = 'block';
            item.addEventListener('mouseout', () => {
              information.style.display = 'none';
              itemOverlay.classList.remove('animation');
            });
          });
        });

        document.querySelector('#phone-selection').addEventListener('change', (event) => {
          let target = event.currentTarget;
          let sku = target.options[target.selectedIndex].value;
          PhoneCatalog.selectItem(phones.find((phone) => phone.sku === sku));
        });
      });
  })
  .catch(alert);
