'use strict';

let arrayChunk = (array, d) => {
  if (array.length <= d) {
    return array;
  }
  var arr = array,
    hold = [],
    ref = -1;
  for (var i = 0; i < arr.length; i++) {
    if (i % d === 0) {
      ref++;
    }
    if (typeof hold[ref] === 'undefined') {
      hold[ref] = [];
    }
    hold[ref].push(arr[i]);
  }

  return hold;
};

let convertToColumnsData = (phones) => {
  var columnize = arrayChunk(phones, 5);
  columnize.forEach((columns, columnIndex) => {
    columns.forEach((phone) => {
      var largeIndex = (columnIndex >= columnize.length - 1) ? 0 : columnIndex + columnIndex;
      var largeItem = columnize[columnIndex][largeIndex];
      phone.className = 'small';
      if (largeItem) {
        largeItem.className = 'large';
      }
    });
  });
  return columnize;
};

class PhoneCatalog {
  selectItem(phone) {
    let itemTarget = document.querySelector('.phone-item[data-sku="' + phone.sku + '"]');
    let fullOverlay = document.querySelector('.full-overlay');

    itemTarget.className += ' focus-item';
    itemTarget.addEventListener('click', () => {
      itemTarget.classList.remove('focus-item');
      fullOverlay.style.display = 'none';
    });

    fullOverlay.addEventListener('click', () => {
      itemTarget.classList.remove('focus-item');
      fullOverlay.style.display = 'none';
    });

    document.addEventListener('click', () => {
      itemTarget.classList.remove('focus-item');
      fullOverlay.style.display = 'none';
    });

    fullOverlay.style.display = 'block';
  }
  loadAllImages(phones) {
    return Promise.all((phones).map((phone) => {
      return new Promise((resolve) => {
        var image = new Image();
        image.src = phone.image;
        image.onload = (data) => resolve(data);
        image.onerror = (error) => resolve(error);
      });
    }));
  }
  render(phones) {
    let columnTemplate = '';
    convertToColumnsData(phones).forEach((columns, columnIndex) => {
      columnTemplate += `
      <div class="col-md-3 column-${columnIndex}">
        <div class="row">
        </div>
      </div>`;
      document.querySelector('#app .row.catalog').innerHTML = columnTemplate;
      let itemTemplate = '';
      columns.forEach((phone) => {
        let gridSize = (phone.className === 'small') ? 6 : 12;
        itemTemplate += `
          <div class="col-md-${gridSize} ${phone.className} phone-item" data-sku="${phone.sku}" style="background-image: url('${phone.image}');">
            <div class="item-overlay"></div>
            <section class="information">
              <div class="name">${phone.name}</div>
              <div class="price">EUR ${phone.price}</div>
            </section>
          </div>
        `;
      });
      return new Promise((resolve) => {
        setTimeout(() => {
          document.querySelector('.column-' + columnIndex + ' .row').innerHTML = itemTemplate;
          resolve();
        }, 0);
      });
    });
  }
}

export default new PhoneCatalog();
