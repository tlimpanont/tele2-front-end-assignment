'use strict';

class PhoneSelector {
  contructor() {

  }
  render(phones) {
    let template = '<option selected>SELECT PHONE</option>';

    phones.forEach((phone) => {
      template += `
          <option value="${phone.sku}">${phone.name}</option>
        `;
    });
    return new Promise((resolve) => {
      document.querySelector('select#phone-selection').innerHTML = template;
      resolve();
    });
  }
}

export default new PhoneSelector();
